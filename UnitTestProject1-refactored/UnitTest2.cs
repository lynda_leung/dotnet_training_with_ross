﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Linq;
using System.Diagnostics;

namespace UnitTestProject1_refactored
{
//This tests the running order of the unit test attributes e.g. [TestInitialize]

    [TestClass]
    public class UnitTestRunOrder
    {
        public UnitTestRunOrder()
        {
            Console.WriteLine("Class constructor");
        }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            Console.WriteLine("ClassInitialize - executes once before the 1st test is run");
        }

        [TestInitialize]
        public void TestInitialize()
        {
            Console.WriteLine("TestInitialize - runs before each test");
        }

        [TestMethod]
        public void TestMethod1()
        {
            Console.WriteLine("TestMethod1");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Console.WriteLine("TestMethod2");
        }

        [TestMethod]
        public void TestMethod3()
        {
            Console.WriteLine("TestMethod3");
        }

        [TestMethod]
        public void TestMethod4()
        {
            Console.WriteLine("TestMethod4");
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            Console.WriteLine("Class cleanup - runs code after ALL tests in the test class have run ");
        }

        [TestCleanup]
        public void TestCleanup()
        { Console.WriteLine("TestCleanup - runs after each test has run. For returning the environment to a known state"); }


    }
}
