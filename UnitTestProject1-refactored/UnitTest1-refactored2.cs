﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Linq;
using System.Diagnostics;

//Added lines so that Ross can review via a PR
namespace UnitTestProject1_refactored
{
    [TestClass]
    public class RefactoredTests2
    {
        private static string baseUrl;
        // private static string bearerToken;
        private static RestClient client;

        private string testInitUserid; //to be used in TestInitialize
              


        //executes once before the 1st test in the class is run 
        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // setup test data, authenticate etc
            baseUrl = "http://amido-tech-test.herokuapp.com";

            // set up the rest client, can do things like add default headers, authentication, etc
            client = new RestClient(baseUrl);

            var tokenService = new TokenService(baseUrl);
            var token = tokenService.GetToken();

            client.AddDefaultHeader("Authorization", $"Bearer {token}");
        }

        [TestInitialize]
        public void TestInit()
        {
            //Creates user and get the userid to be used in tests in suite                
            var userDTO = new UserDto();
            userDTO.name = "Joe";
            userDTO.password = "MyCurrentPassword";

            var createUserRequest = new UserService(client);

            var createUserResponse = createUserRequest.CreateUser(userDTO);

            testInitUserid = (createUserResponse.Headers[4].Value)
              .ToString()
              .Replace("http://amido-tech-test.herokuapp.com/users/", "");
        }

        [TestMethod]
        public void A_CreateUser()
        {
            var userDTO = new UserDto();
            userDTO.name = "Joe";
            userDTO.password = "MyCurrentPassword";

            var createUserRequest = new UserService(client);
            var createUserResponse = createUserRequest.CreateUser(userDTO);

            Assert.AreEqual(HttpStatusCode.Created, createUserResponse.StatusCode);
            Assert.IsNotNull(createUserResponse.Headers[4], "Location header should be in the response - example failure msg");
        }

        [TestMethod]
        public void B_GetUser()
        {
            var getUserRequest = new UserService(client);
            var userName = getUserRequest.GetUser(testInitUserid);
            
            Assert.AreEqual("Joe", userName);
        }

        [TestMethod]
        public void C_UpdateUserPassword()
        {
            var userDTO = new UserDto();
            userDTO.name = "Joe";
            userDTO.password = "MyNewPassword";

            var updateUserRequest = new UserService(client);
            
            var updateUserResponse = updateUserRequest.UpdateUserPassword(userDTO, testInitUserid);

            Assert.AreEqual(HttpStatusCode.OK, updateUserResponse.StatusCode);
            Assert.AreEqual("MyNewPassword", (JObject.Parse(updateUserResponse.Content)).SelectToken("password"));
        }

        [TestMethod]
        public void D_DeleteUser()
        {
            var deleteUserRequest = new UserService(client);

            var deleteUserResponse = deleteUserRequest.DeleteUser(testInitUserid);

            Assert.AreEqual(HttpStatusCode.NoContent, deleteUserResponse.StatusCode);
        }

        [ClassCleanup]
        public static void TestCleanup()
        {
            Console.WriteLine("Class cleanup");
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            Console.WriteLine("Test cleanup");
        }
    }
}
