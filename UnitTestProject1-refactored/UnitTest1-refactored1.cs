﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Net;


namespace UnitTestProject1_refactored
{
    [TestClass]
    public class RefactoredTests1
    {
        private static string baseUrl;
        // private static string bearerToken;
        private static RestClient client;


        //executes once before the 1st test in the class is run 
        [ClassInitialize]
        public static void TestSetup(TestContext context)
        {
            // setup test data, authenticate etc
            baseUrl = "http://amido-tech-test.herokuapp.com";

            // set up the rest client, can do things like add default headers, authentication, etc
            client = new RestClient(baseUrl);

            var tokenService = new TokenService(baseUrl);
            var token = tokenService.GetToken();

            client.AddDefaultHeader("Authorization", $"Bearer {token}");
        }

        [TestInitialize]
        public void TestInit()
        {
            //runs before each test method
        }

        [TestMethod]
        public void A_CreateUser()

        {
            var joe = new
            {
                name = "Joe",
                password = "MyCurrentPassword"
            };

            var createUserRequest = new RestRequest("users", Method.POST);

            createUserRequest.AddJsonBody(joe);

            var createUserResponse = client.Execute(createUserRequest);
            Assert.AreEqual(HttpStatusCode.Created, createUserResponse.StatusCode);
            Assert.IsNotNull(createUserResponse.Headers[4], "Location header should be in the response - example failure msg");

            //can only used the below to pass onto other tests if guaranteed tests will run in a certain order
            var userId = (createUserResponse.Headers[4].Value)
                .ToString()
                .Replace("http://amido-tech-test.herokuapp.com/users/", "");
   
        }

        [TestMethod]
        public void B_GetUser()
        {
            var userId = "59374fh3rhfjsjjjakskw8w";

            var getUserRequest = new RestRequest("users/{userId}", Method.GET);

            getUserRequest
                .AddUrlSegment("userId", userId);

            var getUserResponse = client.Execute(getUserRequest);

            var userName = (JObject.Parse(getUserResponse.Content)).SelectToken("name");
            Assert.AreEqual("Joe", userName);
        }

        [TestMethod]
        public void C_UpdateUserPassword()
        {
            var newPasswordRequestBody = new
            {
                name = "Joe",
                password = "MyNewPassword"
            };


            var userId = "59374fh3rhfjsjjjakskw8w";

            var updatePasswordRequest = new RestRequest("users/{userId}", Method.PUT);

            updatePasswordRequest
                .AddUrlSegment("userId", userId)
                .AddJsonBody(newPasswordRequestBody);

            var updatePasswordResponse = client.Execute(updatePasswordRequest);

            Assert.AreEqual(HttpStatusCode.OK, updatePasswordResponse.StatusCode);
            Assert.AreEqual("MyNewPassword", (JObject.Parse(updatePasswordResponse.Content)).SelectToken("password"));
        }

        [TestMethod]
        public void D_DeleteUser()
        {
            var userId = "59374fh3rhfjsjjjakskw8w";

            var deleteUserRequest = new RestRequest("users/{userId}", Method.DELETE);
            deleteUserRequest
                .AddUrlSegment("userId", userId);

            var deleteUserResponse = client.Execute(deleteUserRequest);
            Assert.AreEqual(HttpStatusCode.NoContent, deleteUserResponse.StatusCode);

            var checkUserIsDeletedRequest = new RestRequest("users/{userId}", Method.GET);
            checkUserIsDeletedRequest
                .AddUrlSegment("userId", userId);

          
            var checkUserIsDeletedResponse = client.Execute(checkUserIsDeletedRequest);
            Assert.AreEqual(HttpStatusCode.NotFound, checkUserIsDeletedResponse.StatusCode);
        }

        [ClassCleanup]
        public static void TestCleanup()
        {
            Console.WriteLine("Class cleanup");
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            Console.WriteLine("Test cleanup");
        }
    }
}
