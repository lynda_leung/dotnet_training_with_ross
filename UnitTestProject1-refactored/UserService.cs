﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1_refactored
{
    class UserService
    {
        private RestClient client;
        

        public UserService(string baseUrl)
        {
            client = new RestClient(baseUrl);
            var tokenService = new TokenService(baseUrl);
            var token = tokenService.GetToken();

            client.AddDefaultHeader("Authorization", $"Bearer {token}");
        }

        public UserService(RestClient client)
        {
            this.client = client;
        }

        public IRestResponse CreateUser(UserDto userDetails)
        {
            //var jsonBody = JsonConvert.SerializeObject(userDetails);            
            var formData = new { name = userDetails.name, password = userDetails.password };
            var createUserRequest = new RestRequest("users", Method.POST);

            createUserRequest.AddJsonBody(formData);

            return client.Execute(createUserRequest);

            //the return client.Execute(createUserRequest); is the same as below 2 lines
            //var createUserResponse = client.Execute(createUserRequest);
            //return createUserResponse;

        }

        public string GetUser(string id)
        {
            var getUserRequest = new RestRequest("users/{userId}", Method.GET);

            getUserRequest
                .AddUrlSegment("userId", id);

            var getUserResponse = client.Execute(getUserRequest);

            var userName = (JObject.Parse(getUserResponse.Content)).SelectToken("name");
            return userName.ToString();
        }

        public IRestResponse UpdateUserPassword(UserDto userDetails, string id)
        {
            var formData = new { name = userDetails.name, password = userDetails.password };

            var updateUserRequest = new RestRequest("users/{userId}", Method.PUT);

            updateUserRequest
                .AddUrlSegment("userId", id)
                .AddJsonBody(formData);

            return client.Execute(updateUserRequest);
        }

        public IRestResponse DeleteUser(string id)
        {

            var deleteUserRequest = new RestRequest("users/{userId}", Method.DELETE);

            deleteUserRequest
                .AddUrlSegment("userId", id);
            
            return client.Execute(deleteUserRequest);
        }
    }

    public class UserDto
    {
        public string name { get; set; }
        public string password { get; set; }
        public string userId { get; set; }
    }
}
