﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1_refactored
{
    class TokenService
    {
        private RestClient client;

        public TokenService(string baseUrl)
        {
            client = new RestClient(baseUrl);
        }

        public string GetToken()
        {
            var tokenRequest = new RestRequest("token");
            var tokenResponse = client.Execute(tokenRequest);
            var tokenResponseContent = tokenResponse.Content;
            var tokenResponseJson = JObject.Parse(tokenResponseContent);
            var token = tokenResponseJson.SelectToken("token");
            return token.ToString();
        }
    }
}
