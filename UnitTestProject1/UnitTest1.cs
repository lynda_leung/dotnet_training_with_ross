﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Linq;
using System.Diagnostics;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
       // String token;
        

        [TestMethod]
        public void TestMethod1()
        {
            // get token

            var client = new RestClient("http://amido-tech-test.herokuapp.com"); // just put base url in this part
            var tokenRequest = new RestRequest("token");
            /* 
               The above is the equivalent of
               var request = new RestRequest();
               request.Resource = "token";
               request.Method = Method.GET;
            */

            var response = client.Execute(tokenRequest);
            var content = response.Content;

            System.Console.WriteLine(content);

            /*
              Add token to header
              1 turn string content into json
              2 what is parse again???
              3 SelectToken doesn't actually mean select token itself, just mean select the value where attribute name is token
            */

            var json = JObject.Parse(content);
            var token = json.SelectToken("token");
            var bearerToken = "Bearer " + token;
            System.Console.WriteLine(bearerToken);

            // create user

            var joe = new
            {
                name = "Joe",
                password = "MyCurrentPassword"
            };

            var createUserRequest = new RestRequest("users", Method.POST);

            //here we chain the methods rather then having to write out createUserRequest.abc each time
            createUserRequest
                .AddHeader("authorization", bearerToken)
                .AddJsonBody(joe);
            var createUserResponse = client.Execute(createUserRequest);
            Assert.AreEqual(HttpStatusCode.Created, createUserResponse.StatusCode);

            var responseHeaders = createUserResponse.Headers;
            string deserialize = JsonConvert.SerializeObject(responseHeaders);
            Console.WriteLine($"response:  {deserialize}");


            //foreach(var header in createUserResponse.Headers)
            //{
            //    Console.WriteLine(header.Name + ": " + header.Value);
            //    Console.WriteLine("{0}: {1}", header.Name, header.Value);
            //    Console.WriteLine($"{header.Name}: {header.Value}"); // <- this is the best!!!!
            //}


            // get user location

            var location = createUserResponse.Headers[4].Value;
            Console.WriteLine($"Location by index: {location}");

            var location2 = createUserResponse.Headers.First(header => header.Name == "Location").Value;
            Console.WriteLine($"Location with LINQ: {location2}");

            var id = location2.ToString().Replace("http://amido-tech-test.herokuapp.com/users/", "");
            Console.WriteLine(id);

            var url = new Uri(location2.ToString());
            var id2 = url.Segments.Last();
            Debug.WriteLine(id2);

            /******* check user created *******/
            //0. set the request headers etc
            //1. retrieve user detail using the GET request plus userid
            //2. extract the value for token = name
            //3. assert that name retrieve = joe

            var getUserRequest = new RestRequest("users/{userId}", Method.GET);

            getUserRequest
                .AddUrlSegment("userId", id)
                .AddHeader("authorization", bearerToken);  //sets up request

            var getUserResponse = client.Execute(getUserRequest);

            var userName = (JObject.Parse(getUserResponse.Content)).SelectToken("name");
            Assert.AreEqual("Joe", userName);

            //previous 2 lines are the equivalent of
            //var userContent = getUserResponse.Content;
            //var userContentJson = JObject.Parse(userContent);
            //var userName = userContentJson.SelectToken("name");
            //Assert.AreEqual("Joe", userName);

            /******* update user password *******/

            var newPasswordRequestBody = new
            {
                name = "Joe",
                password = "MyNewPassword"
            };

            var updatePasswordRequest = new RestRequest("users/{userId}", Method.PUT);
            updatePasswordRequest
                .AddUrlSegment("userId", id)
                .AddHeader("authorization", bearerToken)
                .AddJsonBody(newPasswordRequestBody);

            var updatePasswordResponse = client.Execute(updatePasswordRequest);

            // check password update successful
            Assert.AreEqual(HttpStatusCode.OK, updatePasswordResponse.StatusCode);
            Assert.AreEqual("MyNewPassword", (JObject.Parse(updatePasswordResponse.Content)).SelectToken("password"));



            /******* delete user *******/

            var deleteUserRequest = new RestRequest("users/{userId}", Method.DELETE);
            deleteUserRequest
                .AddUrlSegment("userId", id)
                .AddHeader("authorization", bearerToken);

            var deleteUserResponse = client.Execute(deleteUserRequest);
            Assert.AreEqual(HttpStatusCode.NoContent, deleteUserResponse.StatusCode);

            /******* Check User has been deleted *******/
            /******* This will fail, the assert looks for 404 but 200 is returned as user has not been deleted *******/
            /******* So this test will find a bug in the app - comment out for the time being *******/

            //var checkUserIsDeletedRequest = new RestRequest("users/{userId}", Method.GET);
            //checkUserIsDeletedRequest
            //    .AddUrlSegment("userId", id)
            //    .AddHeader("authorization", bearerToken);

            //var checkUserIsDeletedResponse = client.Execute(checkUserIsDeletedRequest);
            //Assert.AreEqual(HttpStatusCode.NotFound, checkUserIsDeletedResponse.StatusCode);
        }
    }
}
 